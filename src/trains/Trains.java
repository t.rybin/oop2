import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

class Trains {

    public final ArrayList<Train> trainsList = new ArrayList<>();



    void addAirlineToAirlineList(Train train) {
        trainsList.add(train);
    }

    List<Train> getListByNumber() {
        List<Train> list = new ArrayList<>(trainsList);
        list.sort(Comparator.comparingInt(Train::getNumber));
        return list;
    }

    static List<Train> getListByNumber1(ArrayList<Train> trainsList, int number1) {
        List<Train> list = new ArrayList<>();
        for (Train train : trainsList) {
            if (train.getNumber() == number1) {
                list.add(train);
            }
        }

        return list;
    }
    public static Integer inputIntFromConsole() {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter value: ");
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Invalid input. Enter the first value: ");
        }
        value = sc.nextInt();
        return value;
    }
    List<Train> getListByName() {
        List<Train> list = new ArrayList<>(trainsList);
        list.sort((o1, o2) -> {
            int temp;
            if (o1.getDestination().compareToIgnoreCase(o2.getDestination()) == 0) {
                temp = o1.getTime().compareToIgnoreCase(o2.getTime());
            } else {
                temp = o1.getDestination().compareToIgnoreCase(o2.getDestination());
            }
            return temp;
        });
        return list;
    }
}
