import java.util.List;

public class TrainMain {
    public static void main(String[] args) {
        Trains timetable = new Trains();
        timetable.addAirlineToAirlineList(new Train("Minsk", 2, "16:50"));
        timetable.addAirlineToAirlineList(new Train("Minsk", 9, "14:30"));
        timetable.addAirlineToAirlineList(new Train("Gomel", 7, "06:15"));
        timetable.addAirlineToAirlineList(new Train("Brest", 5, "15:00"));
        timetable.addAirlineToAirlineList(new Train("Vilnius", 16, "12:30"));
        timetable.addAirlineToAirlineList(new Train("Vene", 1, "07:30"));

        System.out.println("\nprint trains by number :");
        List<Train> listSortByNumber = timetable.getListByNumber();
        for (Train train : listSortByNumber) {
            System.out.println(train);
        }
        System.out.println("\nprint train by number :");
        List<Train> listSortByNumber1 = Trains.getListByNumber1(timetable.trainsList, Trains.inputIntFromConsole());
        for (Train train : listSortByNumber1) {
            System.out.println(train);
        }
        System.out.println("\nsort by name:");
        List<Train> listSortByName = timetable.getListByName();
        for (Train train : listSortByName) {
            System.out.println(train);
        }
    }
}
